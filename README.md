### 项目启动
## npm run serve or yarn serve


### 初始化

npm -v
```
# npm 6.x
$ npm init vite@latest <project-name> --template vue

# npm 7+，需要加上额外的双短横线
$ npm init vite@latest <project-name> -- --template vue

$ cd <project-name>
$ npm install
$ npm run dev
```
### setup
[语法糖高级写法](https://v3.cn.vuejs.org/api/sfc-script-setup.html#%E5%9F%BA%E6%9C%AC%E8%AF%AD%E6%B3%95)

### 实现功能

- 登录 / 注销

- 权限验证
  - 页面权限
  - 指令权限

- 打包 发布 部署
  - gzip nginx 
  - anywhere

- 全局功能
  - 国际化多语言
  - 动态侧边栏（支持多级路由嵌套）
  - Svg Sprite 图标
  - 组件通讯
  - 组件缓存
  - ECharts 图表
  - websocket 推送

### 项目目录
  ├── dist                       # 打包文件
  ├── node_modules               # 项目依赖
  ├── public                     # 静态资源
  │   │── ie.html                # 兼容页面
  ├── src                        # 源代码
  │   ├── api                    # 所有请求
  │   ├── components             # 全局公用组件
  │   ├── directive              # 全局指令
  │   ├── lang                   # 国际化 language
  │   ├── mixins                 # 混入方法
  │   ├── plugins                # 第三方
  │   ├── router                 # 路由
  │   ├── store                  # 全局 store管理
  │   ├── style                  # 全局样式
  │   ├── utils                  # 全局公用方法
  │   ├── views                  # views 所有页面
  │   ├── App.vue                # 入口页面
  │   ├── main.js                # 入口文件 加载组件 初始化等
  ├── vite                       # vite 配置
  ├── .env.xxx                   # 环境变量配置
  ├── index.html                 # html模板
  ├── vite.config                # vite 配置
  └── package.json               # package.json



### 兼容性
## 浏览器
IE11已不兼容  =》Vue 2.0+

vite需要适配vue3+的组件
vue-router     安装  npm install vue-router@4 -s (直接npm install vue-router  是3.+最新版的)
vuex       npm install vuex@next

sass           npm install sass -D   yarn add -D sass
vue-i18n       npm install vue-i18n@next  (直接安装是8.+最新版  next是适配vue3的) 
## 服务端渲染
nuxt3   

### why vue3
# 更小
1.全局的API和内置组件/功能支持tree-shaking（可以理解为把无用代码摇掉，vue2是全局注册，里面有很大一部分声明的组件并未使用）
2.常驻的代码尺寸控制在10Kb gzipped上下 (gzip打包 )

<!-- const CompressionPlugin = require('compression-webpack-plugin')
configureWebpack: config => {
        if (process.env.NODE_ENV === 'production') {
            return {
                plugins: [
                    new CompressionPlugin({
                        algorithm: 'gzip',
                        test: /\.(js|css)$/,// 匹配文件名
                        threshold: 10240, // 对超过10k的数据压缩
                        deleteOriginalAssets: false, // 不删除源文件
                        minRatio: 0.8 // 压缩比
                    })
                ]
            }
        }
    }
  #nginx   
  gzip on; 
  gzip_static on;
  gzip_buffers 4 16k;
  gzip_comp_level 8;
  gzip_types text/plain application/javascript text/css application/xml text/javascript;
    
-->
# 更快
1.基于Proxy的变动侦测，性能优于getter/setter   
   vue2:definePerproty的定义get和set来替换有状态对象的属性，实现响应式更新。近年来主流浏览器增强了对js新特性的支持，比如proxy，允许拦截针对对象属性的操作，用proxy特性可以消除vue现在不能检测新属性添加的限制，提供更好的性能,也代表vue3放弃了兼容ie。
2.Virtual DOM  (虚拟dom) 重构
   vue2是将模板编译成渲染函数来返回虚拟DOM树，遍历比较每个节点的每个属性来确定更新哪些部分。
   vue3修改了vdom的对比算法，进行完整的遍历对比—>只更新vdom绑定了动态数据的部分。 响应式及h函数

# TS 支持  增加代码的健壮性

# reactive API 和 ref API
  都是创建响应式对象  ref是包装对象 有value  reactive 没有value   类似react useState

# template 不限制一个根元素了   但是多个根元素  在模板上加的class  不解析。。

# const { proxy } = getCurrentInstance()
  替代原来的this，配合 app.config.globalProperties 替代 Vue2 中 Vue.prototype.$xxx  使用






## elementui
[新版文档](https://element-plus.gitee.io/zh-CN/component/border.html)

1. el-form  ref和:model不可同名  否则无法输入  文档是TS的  
2. Element Plus 为 app.config.globalProperties 添加了全局方法 $message 、$alert、 $confirm 等方法
3. Element 国际化在app.vue中  暂未像ELEM2 中适配i18n的第三方调用


## vite 环境变量

import.meta.env  (不再是 process.env)

文件为  .env.development
	    .env.production

静态文件 目录  直接"@/"  (不再需要 "~@"或 require 加载assets里面的图片)



组件传值坑：
emit("on-callback", props['message'] + " " + userName.value)
<Common :message="hello" @onCallback="callback"></Common>
否则 报错：
Component emitted event "callback" but it is neither declared in the emits option nor as an "onCallback" prop.
组件发出了事件“回调”，但它既没有在emits选项中声明，也没有作为“onCallback”属性声明。
