import { defineConfig, loadEnv } from 'vite'
import createVitePlugins from './vite/plugins'
const { resolve } = require('path')
import { getVersion } from './vite/config'
// console.log(getVersion());

// https://vitejs.dev/config/
export default defineConfig(({ mode, command }) => {
  console.log(command);
  const env = loadEnv(mode, process.cwd())
  return {
    resolve: {
      alias: {
        /*
          路径别名
          若为文件系统路径必须是绝对路径的形式，否则将以别名原样呈现，不会解析为文件系统路径路径 
        */
        '~': resolve(__dirname, './'),
        '/@': resolve(__dirname, './src')
      },
      // https://cn.vitejs.dev/config/#resolve-extensions
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
    },
    plugins: createVitePlugins(env, command === 'serve'),
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "/@/style/css/element/index.scss" as *;@import "/@/style/css/index.scss";@import "/@/style/css/resetElemUI.scss";'
        }
      }
    },

    publicDir: 'public',
    base: "./",
    //vite开发服务器配置
    server: {
      host: 'localhost',
      port: 3000,
      open: true,
      strictPort: false,
      https: false,

      // 反向代理
      proxy: {
        '/api': {
          target: 'http://192.168.11.18:9980/',
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, '')
        },
      }


    },

    //生产模式打包配置
    build: {
      target: "es2015", // 默认 "modules"
      manifest: false,
      outDir: 'dist',
      assetsDir: 'assets',
      rollupOptions: {
        output: {
          // 文件分包
          entryFileNames: `assets/[name]${getVersion()}.js`,
          chunkFileNames: `assets/[name]${getVersion()}.js`,
          assetFileNames: `assets/[name]${getVersion()}.[ext]`,
        }
      }
    },
    define: {
      // 在生产中启用/禁用intlify-devtools和vue-devtools支持，默认为false
      __INTLIFY_PROD_DEVTOOLS__: false
    }
  }
})