import vue from '@vitejs/plugin-vue'

import viteCompression from 'vite-plugin-compression';
import createSvgIcon from './svg-icon'

export default function createVitePlugins (viteEnv, isBuild = false) {
  const vitePlugins = [vue()]
  vitePlugins.push(createSvgIcon(isBuild))
  vitePlugins.push(viteCompression())//gzip打包
  return vitePlugins
}
