import moment from 'moment'
export const getVersion = () => { return moment().format("YYMMDDhmmss") }