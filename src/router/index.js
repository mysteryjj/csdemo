import { createRouter, createWebHashHistory } from 'vue-router';
import { useStore } from "vuex"
import store from "/@/store";

// console.log(store);
import NProgress from "nprogress";

import "nprogress/nprogress.css";
NProgress.configure({
  // 动画方式
  easing: "ease",
  // 递增进度条的速度
  speed: 500,
  // 是否显示加载ico
  showSpinner: false,
  // 自动递增间隔
  trickleSpeed: 200,
  // 初始化时的最小百分比
  minimum: 0.3
});

import { getToken, removeToken } from "/@/utils/auth";

import { index, SLA, find, protect, testVnc } from "./routes";
import formats from 'qs/lib/formats';

export const adminRoute = [
  SLA,
  find,
  protect,
  testVnc
];
export const baseRoute = [
  {
    name: "login",
    path: '/login',
    hidden: true,
    component: () => import('/@/views/others/Login.vue'),
  },
  {
    name: "404",
    hidden: true,
    path: '/:pathMatch(.*)', //vue-router4   去掉了 *  换成了  /:pathMatch(.*)
    component: () => import('/@/views/others/404.vue'),
  },
  {
    name: "cloudVnc",
    hidden: true,
    path: '/cloudVnc',
    component: () => import('/@/views/others/CloudVnc.vue'),
  },
  {
    name: "deviceVnc",
    hidden: true,
    path: '/deviceVnc',
    component: () => import('/@/views/others/DeviceVnc.vue'),
  },
  index,
];
const router = createRouter({
  history: createWebHashHistory(), // hash模式：createWebHashHistory，history模式：createWebHistory
  routes: baseRoute
})

// 适配登录&退出登录
router.beforeEach(async (to, from, next) => {
  // console.log(from, to);
  if (to.path == '/login') {
    removeToken()
    if (router.options.routes.length != baseRoute.length) {//退出登录
      window.location.reload();
      return
    }
    next()
  } else {
    const ws_prefix = store.getters.ws_prefix
    if (!ws_prefix) {
      await store.dispatch('app/setWsPrefix')
    }
    NProgress.start();
    let user = getToken();
    if (!user && to.path != '/login') { //未登录
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {//登录状态跳转
      store.dispatch('user/setUser');

      if (to.path == '/') {
        next({
          path: '/index'
        })
      } else {
        // 动态组件缓存
        if (to.fullPath == "/find") {
          if (from.fullPath == "/index") {
            to.meta.keepAlive = true;
          } else {
            to.meta.keepAlive = false;
          }
        }
        if (router.options.routes.length == baseRoute.length) {
          adminRoute.forEach(item => {
            router.addRoute(item)
          })
          router.options.routes = router.options.routes.concat(adminRoute)
          console.log(to);
          next({ path: to.fullPath, query: to.query }) // hack方法 确保addRoutes已完成  刷新页面触发
        } else {
          next()
        }

      }
    }
  }
  document.title = to.name + "--CloudSure";
});
router.afterEach(() => {
  NProgress.done();
});

export default router