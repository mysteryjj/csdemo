import Home from "/@/views/Home.vue";
import i18n from "/@/lang";
const index = {
  path: '/index',
  component: Home,
  name: 'index',
  svg: 'overview',
  children: [{
    path: '/index',
    component: () => import('/@/views/pages/overview/Index.vue'),
    name: i18n.global.t('test.overview')
  }]
}
const SLA = {
  path: '/sla',
  component: Home,
  name: 'sla',
  svg: 'sla',
  children: [{
    path: '/sla',
    component: () => import('/@/views/pages/SLA/Index.vue'),
    name: 'SLA'
  }]
}
const find = {
  path: '/find',
  component: Home,
  name: 'find',
  svg: 'find',
  children: [{
    path: '/find',
    component: () => import('/@/views/pages/find/Index.vue'),
    name: i18n.global.t('test.find'),
    meta: {
      keepAlive: true
    }
  }]
}
const protect = {
  path: '/protect/imagesLevel',
  component: Home,
  svg: 'protect',
  name: i18n.global.t('test.protect'),
  children: [{
    path: '/protect/imagesLevel',
    component: () => import('/@/views/pages/protect/ImagesLevel.vue'),
    name: i18n.global.t('test.image'),
  }, {
    path: '/protect/fliesLevel',
    component: () => import('/@/views/pages/protect/FilesLevel.vue'),
    name: i18n.global.t('test.file'),
  }]
}

const testVnc = {
  path: '/deviceList',
  component: Home,
  svg: 'recovery',
  name: '设备',
  children: [{
    path: '/deviceList',
    component: () => import('/@/views/pages/DeviceList.vue'),
    name: '测试VNC',
  }]
}

export { index, SLA, find, protect, testVnc }