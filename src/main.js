import { createApp } from 'vue';
import router from "/@/router";
import store from "/@/store";
import directive from "/@/directive";
import ElementPlus from 'element-plus';
import i18n from "/@/lang";
import { i18nt } from "/@/lang";
import globalMoment from "/@/plugins/moment";
import App from '/@/App.vue';

import 'virtual:svg-icons-register'
import SvgIcon from '/@/components/svgIcon/Index.vue'
import elementIcons from '/@/components/svgIcon/svgicon'


import ECharts from 'vue-echarts'
import { use } from "echarts/core";

// 手动引入 ECharts 各模块来减小打包体积
import {
  CanvasRenderer
} from 'echarts/renderers'
import {
  BarChart,
  LineChart
} from 'echarts/charts'
import {
  TitleComponent,
  LegendComponent,
  GridComponent,
  TooltipComponent,
} from 'echarts/components'

use([
  CanvasRenderer,
  BarChart,
  LineChart,
  GridComponent,
  TitleComponent,
  LegendComponent,
  TooltipComponent
]);


const app = createApp(App);
// 自定义指令
directive(app);
// 挂载
app.use(i18n).use(router).use(store).use(ElementPlus).use(elementIcons).mount('#app')

// 注册全局方法
i18nt(app);
globalMoment(app);
// 自定义全局注册组件（也可以使用局部注册）
app.component('v-chart', ECharts)
app.component('svg-icon', SvgIcon)

