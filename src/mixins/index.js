import { ref, computed } from "vue";

export default function () {
  const count = ref(0);
  const double = computed(() => count.value += 2)
  function increment () {
    count.value++;
    console.log('我是mixin内部' + count.value);
  }
  return {
    count,
    double,
    increment
  }
}