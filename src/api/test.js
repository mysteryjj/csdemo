import request from '//@/plugins/axios'

export function showTargetList (query) {
  return request({
    url: '/move/device/showTargetList',
    method: 'get',
    params: query
  })
}

export function getRemoteControl (query) {
  return request({
    url: '/cloud/cloudResource/getRemoteControl',
    method: 'get',
    params: query
  })
}
export function getPlatformStragety (query) {
  return request({
    url: '/cloud/cloudPlatform/getPlatformStragety',
    method: 'get',
    params: query
  })
}

export function getVncConfigByUuid (query) {
  return request({
    url: '/move/device/getVncConfigByUuid',
    method: 'get',
    params: query
  })
}