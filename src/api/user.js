import request from '/@/plugins/axios'

export function getWs_prefix () {
  return request({
    url: '/cloud/cloudResource/getWebSocketAddressPrefix',
    method: 'get'
  })
}

export function login (data) {
  return request({
    url: '/admin/user/login',
    method: 'post',
    data
  })
}

export function getInfo () {
  return request({
    url: '/admin/user/getCurrentUserInfo',
    method: 'get'
  })
}

export function logout () {
  return request({
    url: '/admin/user/logOut',
    method: 'get'
  })
}

export function updateUserFav (data) {
  return request({
    url: '/config/home/updateUserFav',
    method: 'post',
    data
  })
}

export function messageList (query) {
  return request({
    url: '/config/home/userMsg',
    method: 'get',
    params: query
  })
}
export function modifyPass (data) {
  return request({
    url: '/config/userConfig/updateUserPwd',
    method: 'post',
    data
  })
}

export function changeFav (query) {
  return request({
    url: '/config/home/updateUserFav',
    method: 'get',
    params: query
  })
}