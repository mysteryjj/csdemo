/*
* 自定义指令控制权限
*/
import store from '/@/store'
export default {
  mounted (el, binding) {
    const { value } = binding;
    const permissions = store.getters && store.getters.permissions;
    // console.log(value);
    // console.log(permissions);
    if (permissions.indexOf(value) == -1) {//无权限不显示
      el.parentNode && el.parentNode.removeChild(el)
    }
  }
}
