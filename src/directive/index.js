import permission from './permission'
// 在mainjs挂载
export default function directive (app) {
  app.directive('permission', permission)
}