const IMAGE_FORMAT = {
  QCOW2: 'qcow2',
  ISO: 'iso'
};

class CloudPlatform {

  /**
   * 格式化EndPoint
   * @param endpoint
   * @returns {{}}
   */
  formatEndPoint (endpoint) {
    endpoint = JSON.parse(endpoint);
    return {
      keystone: endpoint['IDENTITY'],
      MANAGEMENT: endpoint['MANAGEMENT']
    };
  }

  /**
   * 从平台窗口获取平台id
   * @returns {string} 平台id
   */
  getPidFromWindow () {
    return '';
  }

  /**
   * 获取平台支持的镜像格式
   * @returns {Array<String>} 支持的镜像格式数组
   */
  getSupportedImageFormat () {
    return [];
  }

  /**
   * 镜像上传状态变更处理器
   * @param state 上传状态
   */
  handleUploadStateChange (state) {
    Ext.getCmp("preButton")?.setDisabled(state === 'uploading');
  }

  /**
   * 获取空闲空间
   * @param pid 云平台id
   * @return {BigInteger|null}
   */
  getFreeSpaceFromCloud (pid) {
    return null;
  }

  /**
   * 构建镜像上传参数
   * @param path 分片路径/聚合路径
   * @param fileName 文件名
   * @return {{}} 参数对象
   */
  buildUploadImageParam (path, fileName) {
    return {};
  }

  /**
   * 构建远程控制请求参数
   * @param vmId
   * @return {*}
   */
  buildRemoteControlParam (vmId) {
    return null
  }

  /**
   * 构建操作云主机请求参数
   * 操作包括：启动、停止、删除、获取网络信息、获取安全组信息
   * @param vmId
   * @return {*}
   */
  buildOperateVmParam (vmId) {
    return null;
  }

  /**
   * 构建删除云主机网络请求参数
   * @param vmNetwork
   * @param vm
   * @return {String}
   */
  buildRemoveVmNetworkParam (vmNetwork, vm) {
    return null;
  }

  /**
   * 构建删除云主机网络请求参数(目标主机页面)
   * @param netWork
   * @return {*}
   */
  buildRemoveVmNetworkParamOfTargetHost (netWork) {
    return null;
  }

  /**
   * 是否支持公网
   * @returns {boolean}
   */
  isSupportedPublicNet () {
    return true;
  }

  /**
   * 是否支持安全组
   * @returns {boolean}
   */
  isSupportedSecurityGroup () {
    return true;
  }

  /**
   * 构建操作安全组请求参数
   * 操作包括：添加安全组、删除安全组
   * @param vmId
   * @param securityGroupId
   * @returns {*}
   */
  buildOperateSecurityGroupParam (vmId, securityGroupId) {
    return null;
  }

  /**
   * 构建查询云主机可绑定的安全组请求参数
   * @param vmId
   * @returns {*}
   */
  buildFindVmBindableSecurityGroupParam (vmId) {
    return null;
  }

  /**
   * 是否支持一键创建
   * @returns {boolean}
   */
  isSupportedQuickCreate () {
    return true;
  }

  /**
   * 是否支持配置云主机
   * @returns {boolean}
   */
  isSupportedConfigVm () {
    return true;
  }
}

class OpenStack extends CloudPlatform {

  /**
   *
   * @param endpoint
   * @return {{NETWORK: *, IMAGE: *, COMPUTE: *, VOLUME: *, keystone: *}}
   */
  formatEndPoint (endpoint) {
    endpoint = JSON.parse(endpoint);
    return {
      keystone: endpoint['IDENTITY'],
      IMAGE: endpoint['IMAGE'],
      COMPUTE: endpoint['COMPUTE'],
      NETWORK: endpoint['NETWORK'],
      VOLUME: endpoint['VOLUME']
    };
  }

  getPidFromWindow () {
    return Ext.getCmp('imageUploadOpenStack').platform.id;
  }

  getSupportedImageFormat () {
    return [IMAGE_FORMAT.QCOW2];
  }

  buildUploadImageParam (path, fileName) {
    const suffix = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length);
    return {
      window: Ext.getCmp('imageUploadOpenStack'),
      param: {
        creator: {
          chunkFilePaths: path,   // 分片文件路径
          name: fileName,  // 镜像文件名称
          diskFormat: suffix // 磁盘格式
        }
      }
    }
  }

  buildRemoteControlParam (vmId) {
    return JSON.stringify({
      serverId: vmId
    });
  }

  buildOperateVmParam (vmId) {
    return JSON.stringify({
      serverId: vmId
    });
  }

  buildRemoveVmNetworkParam (vmNetwork, vm) {
    return JSON.stringify({
      networkType: vmNetwork.networkType,
      vmNetworkId: vmNetwork.id
    });
  }

  buildRemoveVmNetworkParamOfTargetHost (netWork) {
    return {
      "networkType": netWork.type,
      "vmNetworkId": netWork.vmNetworkId
    };
  }

  buildOperateSecurityGroupParam (vmId, securityGroupId) {
    return {
      "serverId": vmId,
      "securityGroupIds": [securityGroupId]
    };
  }

  buildFindVmBindableSecurityGroupParam (vmId) {
    return {
      "serverId": vmId
    };
  }
}

class AliCloud extends CloudPlatform {

  getPidFromWindow () {
    return Ext.getCmp('imageUploadAliCloud').platform.id;
  }

  getSupportedImageFormat () {
    return [IMAGE_FORMAT.QCOW2];
  }

  buildUploadImageParam (path, fileName) {
    let bucketName = Ext.getCmp('storageCombo').getValue();
    let system = Ext.getCmp('systemCombo').getValue();
    let osDiskSize = Ext.getCmp('osDiskSize').getValue();

    return {
      window: Ext.getCmp('imageUploadAliCloud'),
      param: {
        bucketName: bucketName,   // 存储名称
        uploadFilePath: path,   // 服务器上镜像的绝对路径
        uploadFileName: fileName,   // 镜像文件名称
        system: system,   // 镜像系统信息,值为linux_centos7和windows10两种
        osDiskSize: osDiskSize   // 系统盘大小,单位:GB
      }
    }
  }

  buildRemoteControlParam (vmId) {
    // 展示修改VNC远程连接密码按钮
    Ext.getCmp("modifyVncPwd").show();
    return vmId;
  }

  buildOperateVmParam (vmId) {
    return vmId;
  }

  buildRemoveVmNetworkParam (vmNetwork, vm) {
    let instanceId = vm.vmId;
    // 私网
    if (vmNetwork.networkType === 'PRIVATE') {
      return JSON.stringify({
        resourceType: 'detachENI',
        instanceId: instanceId,
        networkInterfaceId: vmNetwork.id
      });

    } else if (vmNetwork.networkType === 'PUBLIC') {
      return JSON.stringify({
        resourceType: 'detachEIP',
        instanceId: instanceId,
        allocationId: vmNetwork.id
      });
    }
    return null;
  }

  buildRemoveVmNetworkParamOfTargetHost (netWork) {
    if (netWork.type === "PRIVATE") {
      return {
        "resourceType": "detachENI",
        "instanceId": netWork.vmId,
        "networkInterfaceId": netWork.vmNetworkId
      }
    } else {
      return {
        "resourceType": "detachEIP",
        "instanceId": netWork.vmId,
        "allocationId": netWork.vmNetworkId
      }
    }
  }

  isSupportedPublicNet () {
    return false;
  }

  buildOperateSecurityGroupParam (vmId, securityGroupId) {
    return {
      "securityGroupId": securityGroupId,
      "instanceId": vmId
    };
  }

  buildFindVmBindableSecurityGroupParam (vmId) {
    return {
      instanceId: vmId
    };
  }

  /**
   * 阿里云不提供一键创建云主机功能
   * @returns {boolean}
   */
  isSupportedQuickCreate () {
    return false;
  }

  /**
   * 阿里云不提供配置云主机功能
   * @returns {boolean}
   */
  isSupportedConfigVm () {
    return false;
  }
}

class ZStack extends CloudPlatform {

  getPidFromWindow () {
    return Ext.getCmp('imageUploadZStack').platform.id;
  }

  getSupportedImageFormat () {
    return [IMAGE_FORMAT.QCOW2];
  }

  buildUploadImageParam (path, fileName, imageSize) {
    // 将文件后缀改为小写
    let suffix = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length).toLowerCase();
    fileName = fileName.substring(0, fileName.lastIndexOf('.') + 1) + suffix;

    let backupStorage = Ext.getCmp('backupStorage').getValue();

    return {
      window: Ext.getCmp('imageUploadZStack'),
      param: {
        creator: {
          name: fileName,  // 镜像文件名称
          format: suffix, // 磁盘格式
          url: window.location.protocol + '//' + window.location.host + '/upload/downloadImage?fileName=' + fileName + '&path=' + window.btoa(path) + '&fileSize=' + imageSize,
          backupStorageUuids: [backupStorage]
        }
      }
    }
  }

  buildRemoteControlParam (vmId) {
    return JSON.stringify({
      uuid: vmId
    });
  }

  buildOperateVmParam (vmId) {
    return JSON.stringify({
      uuid: vmId
    });
  }

  buildRemoveVmNetworkParam (vmNetwork, vm) {
    return JSON.stringify({
      networkType: vmNetwork.networkType,
      vmNicUuid: vmNetwork.id
    });
  }

  buildRemoveVmNetworkParamOfTargetHost (netWork) {
    return {
      networkType: netWork.type,
      vmNicUuid: netWork.vmNetworkId
    };
  }

  buildOperateSecurityGroupParam (vmId, securityGroupId) {
    return {
      "serverId": vmId,
      "securityGroupIds": [securityGroupId]
    };
  }

  buildFindVmBindableSecurityGroupParam (vmId) {
    return {
      "serverId": vmId
    };
  }
}

/***
* 照着zstack改造的具体什么用法也不太清楚
*/
class Tencent extends CloudPlatform {

  // 目前tencent没有上传镜像先注掉
  // getPidFromWindow() {
  //     return Ext.getCmp('imageUploadZStack').platform.id;
  // }

  getSupportedImageFormat () {
    return [IMAGE_FORMAT.QCOW2];
  }

  // 目前tencent没有上传镜像先注掉
  // buildUploadImageParam(path, fileName,imageSize) {
  //     // 将文件后缀改为小写
  //     let suffix = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length).toLowerCase();
  //     fileName = fileName.substring(0, fileName.lastIndexOf('.') + 1) + suffix;
  //
  //     let backupStorage = Ext.getCmp('backupStorage').getValue();
  //
  //     return {
  //         window: Ext.getCmp('imageUploadZStack'),
  //         param: {
  //             creator: {
  //                 name: fileName,  // 镜像文件名称
  //                 format: suffix, // 磁盘格式
  //                 url: window.location.protocol + '//' + window.location.host + '/upload/downloadImage?fileName=' + fileName + '&path=' + window.btoa(path) + '&fileSize=' + imageSize,
  //                 backupStorageUuids: [backupStorage]
  //             }
  //         }
  //     }
  // }

  buildRemoteControlParam (vmId) {
    return JSON.stringify({
      uuid: vmId
    });
  }

  buildOperateVmParam (vmId) {
    return JSON.stringify({
      uuid: vmId
    });
  }

  buildRemoveVmNetworkParam (vmNetwork, vm) {
    return JSON.stringify({
      networkType: vmNetwork.networkType,
      vmNicUuid: vmNetwork.id
    });
  }

  buildRemoveVmNetworkParamOfTargetHost (netWork) {
    return {
      networkType: netWork.type,
      vmNicUuid: netWork.vmNetworkId
    };
  }

  buildOperateSecurityGroupParam (vmId, securityGroupId) {
    return {
      "serverId": vmId,
      "securityGroupIds": [securityGroupId]
    };
  }

  buildFindVmBindableSecurityGroupParam (vmId) {
    return {
      "serverId": vmId
    };
  }
}

/***
* 照着zstack改造的具体什么用法也不太清楚
*/
class Huawei extends CloudPlatform {

  // 目前Huawei没有上传镜像先注掉
  // getPidFromWindow() {
  //     return Ext.getCmp('imageUploadZStack').platform.id;
  // }

  getSupportedImageFormat () {
    return [IMAGE_FORMAT.QCOW2];
  }

  // 目前Huawei没有上传镜像先注掉
  // buildUploadImageParam(path, fileName,imageSize) {
  //     // 将文件后缀改为小写
  //     let suffix = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length).toLowerCase();
  //     fileName = fileName.substring(0, fileName.lastIndexOf('.') + 1) + suffix;
  //
  //     let backupStorage = Ext.getCmp('backupStorage').getValue();
  //
  //     return {
  //         window: Ext.getCmp('imageUploadZStack'),
  //         param: {
  //             creator: {
  //                 name: fileName,  // 镜像文件名称
  //                 format: suffix, // 磁盘格式
  //                 url: window.location.protocol + '//' + window.location.host + '/upload/downloadImage?fileName=' + fileName + '&path=' + window.btoa(path) + '&fileSize=' + imageSize,
  //                 backupStorageUuids: [backupStorage]
  //             }
  //         }
  //     }
  // }

  buildRemoteControlParam (vmId) {
    return JSON.stringify({
      uuid: vmId
    });
  }

  buildOperateVmParam (vmId) {
    return JSON.stringify({
      serverId: vmId
    });
  }

  buildRemoveVmNetworkParam (vmNetwork, vm) {
    return JSON.stringify({
      networkType: vmNetwork.networkType,
      vmNicUuid: vmNetwork.id
    });
  }

  buildRemoveVmNetworkParamOfTargetHost (netWork) {
    return {
      networkType: netWork.type,
      vmNicUuid: netWork.vmNetworkId
    };
  }

  buildOperateSecurityGroupParam (vmId, securityGroupId) {
    return {
      "serverId": vmId,
      "securityGroupIds": [securityGroupId]
    };
  }

  buildFindVmBindableSecurityGroupParam (vmId) {
    return {
      "serverId": vmId
    };
  }
}

class VSphere extends CloudPlatform {

  getPidFromWindow () {
    return Ext.getCmp('imageUploadVSphere').platform.id;
  }

  getSupportedImageFormat () {
    return [IMAGE_FORMAT.ISO];
  }

  getFreeSpaceFromCloud (pid) {
    let getFreeSpace = function (pid) {
      let freeSize = null;
      Ext.Ajax.request({
        method: 'post',
        async: false,
        url: MS_prefix + '/cloud/cloudResource/doExtendAction',
        params: {
          platformId: pid,
          action: 'findDataStoreFreeSpace',
          param: JSON.stringify({
            dataStoreId: Ext.getCmp('dataStoreCombo').getValue()
          })
        },
        success: function (response) {
          try {
            freeSize = JSON.parse(response.responseText).result;
          } catch (e) {
            console.error(e);
          }
        },
        fail: function (response) {

        }
      });

      return freeSize;
    };
    return getFreeSpace(pid);
  }

  buildUploadImageParam (path, fileName) {
    let filePath = Ext.getCmp('folderTreePanel').getSelectionModel().selected.get(0).raw.path;
    let combo = Ext.getCmp('dataStoreCombo');
    let record = combo.getStore().findRecord('uuid', combo.getValue());
    return {
      window: Ext.getCmp('imageUploadVSphere'),
      param: {
        chunkFilePaths: path,   // 分片文件路径
        fileName: fileName,  // 镜像文件名称
        remoteFilePath: filePath,
        namedDataStoreRef: {
          name: record.raw.props.name,
          reference: record.raw.reference
        }
      }
    }
  }

  buildOperateVmParam (vmId) {
    return JSON.stringify({
      serverId: vmId
    });
  }

  buildRemoveVmNetworkParam (vmNetwork, vm) {
    return JSON.stringify({
      serverId: vm.vmId,
      deviceKey: vmNetwork.id
    });
  }

  buildRemoveVmNetworkParamOfTargetHost (netWork) {
    return {
      serverId: netWork.vmId,
      deviceKey: netWork.vmNetworkId
    };
  }

  isSupportedPublicNet () {
    return false;
  }

  isSupportedSecurityGroup () {
    return false;
  }
}

const UNKNOWN_PLATFORM = new CloudPlatform();
const PLATFORM = Object.freeze({
  'MSOpenStack': new OpenStack(),
  'MSAliCloud': new AliCloud(),
  'MSZStack': new ZStack(),
  'MSTencent': new Tencent(),
  'MSHuawei': new Huawei(),
  'MSVSphere': new VSphere()
});

class CloudPlatformHelper {
  /**
   *
   * @param identifier
   * @returns {OpenStack | AliCloud | ZStack | Tencent | Huawei | VSphere | CloudPlatform}
   */
  static switchByIdentifier (identifier) {
    return PLATFORM[identifier] || UNKNOWN_PLATFORM;
  }
}

export default CloudPlatformHelper