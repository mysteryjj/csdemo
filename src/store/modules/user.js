import { getInfo } from '/@/api/user'
const state = {
  userId: "",
  userName: "",
  userLoginType: 0,
  permissions: []
}

const mutations = {
  SET_USER: (state, data) => {
    state.userId = data.userId;
    state.userName = data.userName;
    state.userLoginType = data.userLoginType;
    state.permissions = data.currentUserPower;
  },
}

const actions = {
  setUser ({ commit }, data) {
    getInfo().then(res => {
      // console.log(res)
      commit('SET_USER', {
        userId: res.CurrentUser.userId,
        userName: res.CurrentUser.userName,
        userLoginType: res.CurrentUser.userLoginType,
        currentUserPower: res.currentUserPower
      })
    })

  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}