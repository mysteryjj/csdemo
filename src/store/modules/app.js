import Cookies from 'js-cookie'
import { getLanguage } from '/@/lang/index'
import { getWs_prefix } from "/@/api/user"

const state = {
  language: getLanguage(),
  ws_prefix: null,
}

const mutations = {
  SET_LANGUAGE: (state, language) => {
    state.language = language
    Cookies.set('i18n', language)
  },
  SET_WS_PREFIX: (state, address) => {
    state.ws_prefix = address
  }
}

const actions = {
  setLanguage ({ commit }, language) {
    commit('SET_LANGUAGE', language)
  },
  async setWsPrefix ({ commit }) {
    try {
      const res = await getWs_prefix()
      console.log(res);
      const prefix = res.prefix ? res.prefix : 'ws://localhost:9980/websocket/'
      commit('SET_WS_PREFIX', prefix)
    } catch (err) {
      console.log(`err`, err)
    }

  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
