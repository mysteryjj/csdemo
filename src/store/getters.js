const getters = {
  language: state => state.app.language,
  ws_prefix: state => state.app.ws_prefix,
  permissions: state => state.user.permissions,
  userName: state => state.user.userName,
  userId: state => state.user.userId,
}
export default getters
