import { createStore } from 'vuex'
import getters from './getters'


// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = import.meta.globEager('./modules/*.js')

const modules = {};

for (const key in modulesFiles) {
  // console.log(key);
  let moduleName = key.substr(key.lastIndexOf("/") + 1);
  if (Object.prototype.hasOwnProperty.call(modulesFiles, key)) {
    modules[moduleName.replace(/(\.\/|\.js)/g, '')] = modulesFiles[key].default
  }
}
// console.log(modules);

const store = createStore({
  modules,
  getters
})

export default store
